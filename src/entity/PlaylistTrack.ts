import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Playlist } from './Playlist';
import { Track } from './Track';

@Entity('playlist_track')
export class PlaylistTrack {
  @PrimaryColumn()
  PlaylistId: number;

  @PrimaryColumn()
  TrackId: number;

  @ManyToOne(() => Playlist, (playlist) => playlist.playlistTracks)
  @JoinColumn({ name: 'PlaylistId' })
  playlist: Playlist;

  @ManyToOne(() => Track, (track) => track.playlistTracks)
  @JoinColumn({ name: 'TrackId' })
  track: Track;
}
