import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Track } from './Track';

@Entity('genres')
export class Genre {
  @PrimaryGeneratedColumn()
  GenreId: number;

  @Column({ type: 'nvarchar', length: 120, nullable: true })
  Name: string;

  @OneToMany(() => Track, (track) => track.genre)
  tracks: Track[];
}
