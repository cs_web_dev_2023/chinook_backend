import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Artist } from './Artist';
import { Track } from './Track'; // Assuming you have a Track entity

@Entity('albums')
export class Album {
  @PrimaryGeneratedColumn()
  AlbumId: number;

  @Column({ type: 'nvarchar', length: 160 })
  Title: string;

  @Column()
  ArtistId: number;

  @ManyToOne(() => Artist, (artist) => artist.albums, {
    cascade: true, // Or as needed based on your business logic
    onDelete: 'NO ACTION', // Reflecting the SQL schema's constraint
    onUpdate: 'NO ACTION',
  })
  @JoinColumn({ name: 'ArtistId' })
  artist: Artist;

  @OneToMany(() => Track, (track) => track.album)
  tracks: Track[];
}
