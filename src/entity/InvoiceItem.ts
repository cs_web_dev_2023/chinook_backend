import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Invoice } from './Invoice';
import { Track } from './Track';

@Entity('invoice_items')
export class InvoiceItem {
  @PrimaryGeneratedColumn()
  InvoiceLineId: number;

  @Column()
  InvoiceId: number;

  @Column()
  TrackId: number;

  @Column('numeric', { precision: 10, scale: 2 })
  UnitPrice: number;

  @Column()
  Quantity: number;

  @ManyToOne(() => Invoice, (invoice) => invoice.invoiceItems)
  @JoinColumn({ name: 'InvoiceId' })
  invoice: Invoice;

  @ManyToOne(() => Track, (track) => track.invoiceItems)
  @JoinColumn({ name: 'TrackId' })
  track: Track;
}
