import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { PlaylistTrack } from './PlaylistTrack';

@Entity('playlists')
export class Playlist {
  @PrimaryGeneratedColumn()
  PlaylistId: number;

  @Column({ type: 'nvarchar', length: 120, nullable: true })
  Name: string;

  @OneToMany(() => PlaylistTrack, (playlistTrack) => playlistTrack.playlist)
  playlistTracks: PlaylistTrack[];
}
