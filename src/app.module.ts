import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Album } from './entity/Album';
import { Artist } from './entity/Artist';
import { Customer } from './entity/Customer';
import { Employee } from './entity/Employee';
import { Genre } from './entity/Genre';
import { Invoice } from './entity/Invoice';
import { InvoiceItem } from './entity/InvoiceItem';
import { MediaType } from './entity/MediaType';
import { Playlist } from './entity/Playlist';
import { PlaylistTrack } from './entity/PlaylistTrack';
import { Track } from './entity/Track';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'chinook.db',
      entities: [
        Album,
        Artist,
        Customer,
        Employee,
        Genre,
        Invoice,
        InvoiceItem,
        MediaType,
        Playlist,
        PlaylistTrack,
        Track,
      ],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
