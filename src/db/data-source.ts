import { Customer } from './../entity/Customer';
import { Track } from './../entity/Track';
import { PlaylistTrack } from './../entity/PlaylistTrack';
import { Playlist } from './../entity/Playlist';
import { MediaType } from './../entity/MediaType';
import { InvoiceItem } from './../entity/InvoiceItem';
import { Invoice } from './../entity/Invoice';
import { Genre } from './../entity/Genre';
import { Employee } from './../entity/Employee';

import { Artist } from './../entity/Artist';
import { Album } from './../entity/Album';

import { DataSource } from 'typeorm';

export const AppDataSource = new DataSource({
  type: 'sqlite',
  database: 'chinook.db',
  synchronize: true,
  logging: false,
  entities: [
    Album,
    Artist,
    Customer,
    Employee,
    Genre,
    Invoice,
    InvoiceItem,
    MediaType,
    Playlist,
    PlaylistTrack,
    Track,
  ],
  migrations: [],
  subscribers: [],
});
